<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'dream2b_opcao2');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f.3%69[Uc&HjyIHjcK_nVkVfko<zE&Ff?t?E&.9]+xXZx7F:r+9B/5I!N8l;6o_[');
define('SECURE_AUTH_KEY',  '4MVX@Hb?:C[l[ww3kuhVTP-#2F?_)Tn@[Uh>;X)STReL;xV8SiJoLv<B:zfy^R/F');
define('LOGGED_IN_KEY',    ' gD^&>VI^)?-NB^yfG&S,H]zxcyWabLe:wO~A!(f,nBLy6 -|.RA`>/f/3se7i9c');
define('NONCE_KEY',        'D.Wvc0k>.}FjfVdlt];ZR!pMU0pt]}=a~@@eRUShGK5|<~|jBHx|m;SA#L(DkfVD');
define('AUTH_SALT',        'M*E%v5.3h.`3bN&p?C(X3>?z>B1`({e_!,My+w;at~]P=`fIxA1$Ub)N(Jui,El%');
define('SECURE_AUTH_SALT', 'k| =JzGCPDs~Y/Pgdn~#Z3Hw-3E9ZM:$a`1@IsFiY?U{B~oIW?|$}VCt-KNJMCmk');
define('LOGGED_IN_SALT',   ' rr5RQUpyt7 },E^tjIPe=8CCxwu}TC7xX5WADj>uX_[_CfMT!c1>{_P>8O&+~xc');
define('NONCE_SALT',       'DHi?Q*H0B<lB+22 MiU%=XI}p35b ~@ys<7Ac9(Tc9WB pI[b|a&;SE@h&L1H:Ov');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
